<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Post;

/**
 * @Route("/projets")
 */
class ProjectController extends Controller
{
    
     /**
     * @Route("/", name="projects_index")
     */
    public function indexAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $introFinalizedProjet = $em->getRepository('AppBundle:Post')->oneByCategorySlug('intro-projets-realises');
        $finalizedProjets = $em->getRepository('AppBundle:Post')->byCategorySlug('projets-realises');

        $introPendingProjet = $em->getRepository('AppBundle:Post')->oneByCategorySlug('intro-projets-en-cours');
        $pendingProjets = $em->getRepository('AppBundle:Post')->byCategorySlug('projets-en-cours');

        return $this->render('default\projets.html.twig', array(
            'introFinalizedProjet' => $introFinalizedProjet,
            'finalizedProjets'      => $finalizedProjets,
            'introPendingProjet'   => $introPendingProjet,
            'pendingProjets'        => $pendingProjets
        ));
    }

    /**
     * @Route("/{postSlug}", name="project_show")
     * @Method("GET")
     */
    public function postShowAction(Request $request, $postSlug) 
    {

        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->findOneBySlug($postSlug);
        $relatedPosts = $em->getRepository('AppBundle:Post')->findByCategory($post->getCategory());

        if(!$post) {
            throw $this->createNotFoundException("Aucun projet trouvé.");
            
        }
        
        return $this->render('default\post-show.html.twig', array(
            'post' => $post,
            'relatedPosts' => $relatedPosts
        
        ));
    }

     public function pendingProjectsAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $pendingProjects = $em->getRepository('AppBundle:Post')->byCategorySlug('projets-en-cours');

        return $this->render('includes\pending-projects.html.twig', array(
            'pendingProjects' =>$pendingProjects,
        ));
    }

}
