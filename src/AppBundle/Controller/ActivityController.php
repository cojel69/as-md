<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Post;

/**
 * @Route("/nos-activites")
 */
class ActivityController extends Controller
{
    
    /**
     * @Route("/", name="activity_index")
     */
    public function indexAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $activities = $em->getRepository('AppBundle:Post')->byCategorySlug('nos-activites');

        return $this->render('default\our-activities.html.twig', array(
            'activities' =>$activities
        ));
    }


    /**
     * @Route("/{postSlug}", name="activity_post_show")
     * @Method("GET")
     */
    public function postShowAction(Request $request, $postSlug) 
    {

        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->findOneBySlug($postSlug);
        $relatedPosts = $em->getRepository('AppBundle:Post')->findByCategory($post->getCategory());

        if(!$post) {
            throw $this->createNotFoundException("Aucune activité trouvée.");
            
        }
        
        return $this->render('default\post-show.html.twig', array(
            'post' => $post,
            'relatedPosts' => $relatedPosts
        
        ));
    }

    public function ourActivityAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $activities = $em->getRepository('AppBundle:Post')->byCategorySlug('nos-activites');

        return $this->render('includes\our-activities.html.twig', array(
            'activities' =>$activities
        ));
    }


}
