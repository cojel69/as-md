<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Post;
use AppBundle\Form\PostType;


/**
 * @Route("/post")
* @author Prudence Assogba <jprud67@gmail.com>
*/
class PostController extends Controller
{

    /**
     * @Route("/", name="blog")
     */
    public function blogAction(Request $request) 
    {
        $em = $this->getDoctrine()->getManager();
        $findPosts = $em->getRepository('AppBundle:Post')->postsAll();
        //dump($findPosts->getTranslations());die();
       
        $posts = $this->get('knp_paginator')->paginate($findPosts, $request->query->getInt('page', 1),10);
        return $this->render($this->get('ThemeRooter')->getView('post\index.html.twig'),array(
        'posts' =>$posts,
           ));
    }
	 /*Traitement d'article par catégorie*/
     public function categorylistAction()
    {
         $em = $this->getDoctrine()->getManager();
         $categories = $em->getRepository('AppBundle:Category')->findAll();
        // var_dump($categories);//die();
        return $this->render($this->get('ThemeRooter')->getView('category\categorylist.html.twig'), array(
          'categories' => $categories
          ));
    }
    

    /**
     * @Route("/category/{category}", name="category_posts")
     */
    public function categorypostAction(Request $request,$category) 
    {
        if (isset($category)) {     
           $em = $this->getDoctrine()->getManager();
            $category=$em->getRepository('AppBundle:Category')->findOneBy(['slug'=>$category]);
           $findPosts = $em->getRepository('AppBundle:Post')->categoryPosts($category);
           //dump($category);die();
        }
         else
        {
          return $this->redirectToRoute('blog');
        } 
       
         $posts = $this->get('knp_paginator')->paginate($findPosts, $request->query->getInt('page', 1),10);
         return $this->render($this->get('ThemeRooter')->getView('post\index.html.twig'),array(
        'posts' =>$posts,
         'category'=>$category
           ));
    }

    /**
     * @Route("/tag/{tag}", name="posts_tag")
     */
    public function tagpostAction(Request $request,$tag) 
    {
        if (isset($tag)) {     
           $em = $this->getDoctrine()->getManager();
            $tag=$em->getRepository('AppBundle:Tag')->findOneBy(['slug'=>$tag]);
           $findPosts = $em->getRepository('AppBundle:Post')->tagPosts($tag);
        }
         else
        {
          return $this->redirectToRoute('blog');
        } 
       
         $posts = $this->get('knp_paginator')->paginate($findPosts, $request->query->getInt('page', 1),10);
         return $this->render($this->get('ThemeRooter')->getView('post\index.html.twig'),array(
        'posts' =>$posts,
         'tag'=>$tag
           ));
    }

   //Traitement d'affichage d'un article

    /**
     * @Route("/{slug}",name="blog_post")
     * @ParamConverter("post", options={"mapping": {"slug": "title"}})
     */
    public function blog_postAction(Request $request, $slug)
    {
        $findPosts=[];
        $similarposts=[];
        $em = $this->getDoctrine()->getManager();
        $post= $em->getRepository('AppBundle:Post')->findOneBySlug($slug);
        
        if (empty($post)) {
            return $this->redirectToRoute('blog');
        }
        //Recherce des article similaires
        $postes = $em->getRepository('AppBundle:Post')->findAll();
         //dump($postes,$post);die();
        foreach ( $postes as $title => $value) 
        {
           
            if (strspn($value->getTitle(),  $post->getTitle()) != 0) 
            {
                if ($value->getTitle() != $post->getTitle())
                {
                   $findPosts[]=$value;
                } 
            }
        } 

        if (!empty($findPosts)) 
        {
           $similarposts = $this->get('knp_paginator')->paginate($findPosts, $request->query->getInt('page', 1),3);  
        }
        else
        {
            $similarposts=[];
        }
              
        return $this->render($this->get('ThemeRooter')->getView('post\post.html.twig'),array(
            'post' =>$post,
            'similarposts'   =>$similarposts
            ));

    }

}