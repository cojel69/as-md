<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Post;

/**
 * @Route("/a-propos")
 */
class EventController extends Controller
{
    
     /**
     * @Route("/", name="about")
     */
    public function indexAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $introPost = $em->getRepository('AppBundle:Post')->oneByCategorySlug('a-propos');
        $ceoPost = $em->getRepository('AppBundle:Post')->oneByCategorySlug('president');
        $events = $em->getRepository('AppBundle:Post')->byCategorySlug('actualites');

        return $this->render('default\about.html.twig', array(
            'introPost' =>$introPost,
            'events' =>$events,
            'ceoPost' =>$ceoPost
        ));
    }

    /**
     * @Route("/{postSlug}", name="event_post_show")
     * @Method("GET")
     */
    public function postShowAction(Request $request, $postSlug) 
    {

        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->findOneBySlug($postSlug);
        $relatedPosts = $em->getRepository('AppBundle:Post')->findByCategory($post->getCategory());

        if(!$post) {
            throw $this->createNotFoundException("Aucune actualité trouvée.");
            
        }
        
        return $this->render('default\post-show.html.twig', array(
            'post' => $post,
            'relatedPosts' => $relatedPosts
        
        ));
    }

}
