<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\ContactUs;
use AppBundle\Form\ContactUsType;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function homeGalleryAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $galleryPost = $em->getRepository('AppBundle:Post')->oneByCategorySlug('acceuil');
        $welcomePost = $em->getRepository('AppBundle:Post')->oneByCategorySlug('bienvenue');

        return $this->render('default\index.html.twig', array(
            'galleryPost' =>$galleryPost,
            'welcomePost' =>$welcomePost
        ));
    }

   

     /**
     * @Route("/galerie", name="gallery")
     */
    public function galleryAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $galleryMenu = $em->getRepository('AppBundle:Post')->byCategorySlug('galerie');
        $currentGallery = $em->getRepository('AppBundle:Post')->oneByCategorySlug('galerie');

        return $this->render('default\gallery.html.twig', array(
            'galleryMenu' =>$galleryMenu,
            'currentGallery' =>$currentGallery
        ));
    }

    /**
     * @Route("/galerie/{slug}", name="gallery_category")
     */
    public function galleryCategoryAction($slug) 
    {
             
        $em = $this->getDoctrine()->getManager();
        $galleryMenu = $em->getRepository('AppBundle:Post')->byCategorySlug('galerie');
        $currentGallery = $em->getRepository('AppBundle:Post')->findOneBySlug($slug);

        return $this->render('default\gallery.html.twig', array(
            'galleryMenu' =>$galleryMenu,
            'currentGallery' =>$currentGallery
        ));
    }

    /**
     * @Route("/contactez-nous", name="contact_us")
     */
    public function contactUsAction(Request $request) 
    {
             
        $em = $this->getDoctrine()->getManager();
        $contact = new ContactUs();
        $form = $this->createForm(ContactUsType::class, $contact);
        $address = $em->getRepository('AppBundle:Address')->findOneBy([], ['id'=>'desc']);

        $form->handleRequest($request);

        if($form->IsValid() && $form->isSubmitted())
        {
           $em->persist($contact);
           $em->flush();

           $this->addFlash('success', 'Votre message a bien été envoyé.');

           return $this->redirectToRoute('contact_us');
        }

        return $this->render('default\contact.html.twig', array(
            'address' =>$address,
            'form' =>$form->createView(),
        ));
    }

    
    public function ourContactAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $ourContact = $em->getRepository('AppBundle:Address')->findOneBy([], ['id'=>'desc']);

        return $this->render('includes\our-contact.html.twig', array(
            'ourContact' =>$ourContact,
        ));
    }

     public function socialsLinksAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $address = $em->getRepository('AppBundle:Address')->findOneBy([], ['id'=>'desc']);

        return $this->render('includes\socials-links.html.twig', array(
            'address' =>$address,
        ));
    }

    public function infoBeninAction() 
    {
             
        $em = $this->getDoctrine()->getManager();
        $ourContact = $em->getRepository('AppBundle:Post')->byCategorySlug('');

        return $this->render('includes\our-contact.html.twig', array(
            'ourContact' =>$ourContact,
        ));
    }

    
   public function leBeninLinksAction() 
   {
            
       $em = $this->getDoctrine()->getManager();
       $aboutBenins = $em->getRepository('AppBundle:Post')->byCategorySlug('le-benin');

       return $this->render('includes\le-benin.html.twig', array(
           'aboutBenins' =>$aboutBenins
       ));
   }

   /**
     * @Route("/le-benin/{postSlug}", name="le_benin_show")
     * @Method("GET")
     */
    public function beninPostShowAction($postSlug) 
    {

        $em = $this->getDoctrine()->getManager();
        $post = $em->getRepository('AppBundle:Post')->findOneBySlug($postSlug);
        $relatedPosts = $em->getRepository('AppBundle:Post')->findByCategory($post->getCategory());

        if(!$post) {
            throw $this->createNotFoundException("Aucun projet trouvé.");
            
        }
        
        return $this->render('default\post-show.html.twig', array(
            'post' => $post,
            'relatedPosts' => $relatedPosts
        
        ));
    }

}
