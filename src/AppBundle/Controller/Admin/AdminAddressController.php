<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\Address;
use AppBundle\Form\AddressType;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Route("admin/parametres/infos-site")
 * 
 * @author Arnaud Anato <arnato1@gmail.com>
 */
class AdminAddressController extends Controller
{
    /**
     * controlleur used to manage the admin panel
     * @Route("/", name="admin_address_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $address = $em->getRepository('AppBundle:Address')->findAll();

        return $this->render('admin\index.html.twig',[
            "address"=> $address,
        ]);
    }

    /**
     * Create Address data
     * 
     * @Route("/new", name="admin_address_new")
     */
    public function newAction(Request $request)
    {
        $address = new Address();
        $form = $this->createForm(AddressType::class, $address);

        $form->handleRequest($request);

        if($form->isValid()) {
           //dump($address); die();
           $em = $this->getDoctrine()->getManager();
           $em->persist($address);
           $em->flush();

           $this->addFlash('success', 'Les données de l\'adresse ont bien été créées');

           return $this->redirectToRoute('admin_address_edit', ['id' => $address->getId()]);
        }
        
        return $this->render('admin\address\edit.html.twig',[
            "address"=> $address,
            "form"=> $form->createView(),
        ]);
    }

    /**
     * Edit Address data
     * 
     * @Route("/edit/{id}", name="admin_address_edit")
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $address = $em->getRepository('AppBundle:Address')->find($id);
        $oldSocials = new ArrayCollection();
        $form = $this->createForm(AddressType::class, $address);

        $form->handleRequest($request);

        if($form->isValid()) {
           //dump($address); die();
           
           foreach ($oldSocials as $social) {
               if (!$address->getSocials()->contains($social)) {
                   
                   $social->setAddress(null);

                   $em->remove($social);
               }
           }

           $em->persist($address);
           $em->flush();

           $this->addFlash('success', 'Les données ont bien été sauvées');

           return $this->redirectToRoute('admin_address_edit', ['id' => $address->getId()]);
        }
        
        return $this->render('admin\address\edit.html.twig',[
            "address"=> $address,
            "form"=> $form->createView(),
        ]);
    }

    /**
     * count address in DB
     * 
     * @Route("/fr", name="admin_address_exist")
     */
    public function checkAddressAction()
    {
        $em = $this->getDoctrine()->getManager();

        $address = $em->getRepository('AppBundle:Address')->findAll();

        $new  = $this->generateUrl('admin_address_new');

        if(count($address) > 0) {
            $edit = $this->generateUrl('admin_address_edit', ['id' => $address[0]->getId()]);
            return new Response($edit);
        }
        
        return new Response($new);
    }


}
