<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Post;
use AppBundle\Entity\Translations;
use AppBundle\Form\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Controller used to manage post contents in admin panel.
 *
 * @Route("admin/post")
 *
 * @author Prudence Assogba <jprud67@gmail.com>
 */
class AdminPostController extends Controller
{
    /**
     * Lists all Post entities.
     *
     * @Route("/", name="admin_post_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository('AppBundle:Post')->findAll();
        //$posts = $this->get('knp_paginator')->paginate($findPosts, $request->query->getInt('page', 1),5);
        return $this->render('admin/post/posts_index.html.twig',
                            ['posts' => $posts]);
    }

    /**
     * Creates a new Post entity.
     *
     * @Route("/new", name="admin_post_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //dump($request); die();
        $post = new Post();
        $oldDossierImages = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($post->getDossierImages() as $image) {
            $oldDossierImages->add($image);
        }

        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($oldDossierImages as $image) {
                if (!$post->getDossierImages()->contains($image)) {
                    
                    $image->setPosts(null);

                    $em->remove($image);
                }
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('post.flash.created'));

            return $this->redirectToRoute('admin_post_index');
        }
        //dump('hello'); die();
        return $this->render('admin/post/new_post.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Show a Post entity.
     *
     * @Route("/{id}", requirements={"id": "\d+"}, name="admin_post_show")
     */
    public function showAction(Post $post)
    {
        
        return $this->render('admin/blog/show.html.twig', [
            'post'        => $post,
        ]);
    }

    /**
     * Edit an existing Post entity.
     *
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_post_edit")
     * @Method({"GET", "POST"})
     */
    public function postEditAction(Post $post, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $oldDossierImages = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($post->getDossierImages() as $image) {
            $oldDossierImages->add($image);
        }

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            foreach ($oldDossierImages as $image) {
                if (!$post->getDossierImages()->contains($image)) {
                    
                    $image->setPosts(null);

                    $em->remove($image);
                }
            }

            $em->persist($post);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('post.flash.updated'));

            return $this->redirectToRoute('admin_post_index', ['id' => $post->getId()]);
        }

        return $this->render('admin/post/post_edit.html.twig', [
            'form'   => $form->createView(),
            'id' => $post->getId(),
            "post"=>$post
        ]);
    }

    /**
     * Delete a Post entity.
     *
     * @Route("/{id}/delete", name="admin_post_delete")
     */
    public function deleteAction(Post $post)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($post);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('post.flash.deleted'));
        return $this->redirectToRoute('admin_post_index');
    }

    
}
