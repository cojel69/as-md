<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * @Route("admin")
 * @author Prudence Assogba <jprud67@gmail.com>
 */
class AdminController extends Controller
{
    /**
     * controlleur used to manage the admin panel
     * @Route("/", name="admin_home")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository('AppBundle:Post')->findAll();
        $users = $em->getRepository('AppBundle:User')->findAll();
        return $this->render('admin\index.html.twig',[
            "posts"=> $posts,
            "users"=>$users,
        ]);
    }

    /**
     *delete All Entity datas
     *
     * @Route("/{repos}/select/delete", name="delete_select_data")
     */
    public function deleteselectAction($repos,Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $datas = $em->getRepository($repos)->findAll();
        foreach ($datas as $key => $value) {
            if(null !== $request->request->get($value->getId())){
                $em->remove($value);
            }
        }
        $em->flush();

        $url = $request->headers->get('referer');
        $this->addFlash('success', $this->get('translator')->trans('flash.entity.deleted'));
        return new RedirectResponse($url);
    }


}