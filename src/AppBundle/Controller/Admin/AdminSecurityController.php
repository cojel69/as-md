<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Doctrine\UserManager;
use AppBundle\Form\RegistrationType;
use AppBundle\Form\RoleType;
use AppBundle\Entity\User;
use FOS\UserBundle\Form\Type\RegistrationFormType;
use Symfony\Component\HttpFoundation\Response;


/**
 * Controller used to manage settings contents in admin panel.
 *
 * @Route("admin/users") 
 * @author Prudence Assogba <jprud67@gmail.com>
 */
class AdminSecurityController extends Controller
{
    /**
     * @Route("/", name="admin_user_index")
     */
    public function indexAction(Request $request)
    {
        $users = $this->get('fos_user.user_manager')->findUsers();
        //$users = $this->get('knp_paginator')->paginate($findUsers, $request->query->getInt('page', 1),5);
        return $this->render('admin\security\index_users.html.twig', array(
            'users' => $users));
    }
    
    /**
     * @Route("/new", name="admin_user_new")
     */
    public function newUserAction(Request $request)
    {
        $um = $this->get('fos_user.user_manager');
    	$user = $um->createUser();
    	$form = $this->createForm(RegistrationType::class, $user);
      
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
        
            $um->updateUser($user);

            $this->addFlash('success',  $this->get('translator')->trans('user.flash.created'));
            return $this->redirectToRoute('admin_user_index');
        }
        return $this->render('admin\security\user_new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_user_edit")
     */
    public function UserEditAction(Request $request, User $user)
    {
         $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {            
            $this->get('fos_user.user_manager')->updateUser($user);

            $this->addFlash('success',  $this->get('translator')->trans('user.flash.updated'));
            return $this->redirectToRoute('admin_user_index');
        }

        return $this->render('admin\security\user_edit.html.twig', array(
            'form' => $form->createView(),
            'user' =>$user,
            'id'=>$user->getId()
        ));
    }

    /**
     * @Route("/{id}/delete", requirements={"id": "\d+"}, name="admin_user_delete")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function UserDeleteAction(User $user)
    {
        
        $userManager = $this->get('fos_user.user_manager');
        $userManager->deleteUser($user);

            $this->addFlash('success',  $this->get('translator')->trans('user.flash.deleted'));
        return $this->redirectToRoute('admin_user_index');
    }

    /**
     * @Route("/profile", name="admin_user_profile")
     */
    public function showProfileAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }
    
        return $this->render('admin\security\Profile.html.twig', array(
            'user' => $user
        ));
    }

     /**
     * @Route("/activate/{username}",  name="admin_user_activate")
     */
    public function activateUserAction($username)
    {
        $flash=array();
        $um = $this->get('fos_user.util.user_manipulator');
        $user = $um->activate($username);
        $this->addFlash('success',  $this->get('translator')->trans('user.flash.activate'));
            return $this->redirectToRoute('admin_user_index');
    }
     /**
     * @Route("/deactivate/{username}", name="admin_user_deactivate")
     */
    public function deactivateUserAction($username)
    {
        $flash=array();
        $um = $this->get('fos_user.util.user_manipulator');
        $user = $um->deactivate($username);
         $this->addFlash('success',  $this->get('translator')->trans('user.flash.deactivate'));
            return $this->redirectToRoute('admin_user_index');
    }

    /**
     * @Route("/addrole/{username}/{role}", name="admin_user_addrole")
     */
    public function addRoleUserAction($username,$role)
    {
        $flash=array();
        $um = $this->get('fos_user.util.user_manipulator');
        $user = $um->addRole($username,$role);
         $this->addFlash('success',  $this->get('translator')->trans('user.flash.addrole'));
            return $this->redirectToRoute('admin_user_index');
    }

    /**
     * @Route("/removerole/{username}/{role}", name="admin_user_removerole")
     */
    public function removeRoleUserAction($username,$role)
    {
        $flash=array();
        $um = $this->get('fos_user.util.user_manipulator');
        $user = $um->removeRole($username,$role);
         $this->addFlash('success',  $this->get('translator')->trans('user.flash.removerole'));
            return $this->redirectToRoute('admin_user_index');
    }

    
}
