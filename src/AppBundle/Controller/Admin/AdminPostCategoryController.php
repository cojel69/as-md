<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Category;
use AppBundle\Form\CategoryType;

/**
 * Controller used to manage post's category in admin panel.
 *
 * @Route("admin/category")
 *
 * @author Prudence Assogba <jprud67@gmail.com>
 */
class AdminPostCategoryController extends Controller
{
    /**
     * Lists all Category entities.
     *
     * @Route("/", name="admin_post_category_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
       // $categories = $this->get('knp_paginator')->paginate($findCategories, $request->query->getInt('page', 1),5);
        return $this->render('admin/category/category_list.html.twig',
                            ['categories' => $categories]);
    }

    /**
     * Creates a new Category entity.
     *
     * @Route("/new", name="admin_post_category_new")
     */
    public function newAction(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('postcat.flash.created'));

            return $this->redirectToRoute('admin_post_category_index');
        }

        return $this->render('admin/category/new_category.html.twig',
                            ['form' => $form->createView(),]);
    }

    /**
     * Edit an existing Category entity.
     *
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="admin_post_category_edit")
     */
    public function editAction(Request $request, Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(CategoryType::class, $category);
        //$logs=$this->get('app.loggable')->getList($category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans('postcat.flash.updated'));

            return $this->redirectToRoute('admin_post_category_index');
        }

        return $this->render('admin/category/category_edit.html.twig',
                            ['form' => $form->createView(),
                              "id" => $category->getId(),
                              "category"=>$category,
                              //'logs'=>$logs
                            ]);
    }

    /**
     * Delete a category entity.
     *
     * @Route("/{id}/delete", name="admin_post_category_delete")
     */
    public function deleteAction(Category $category)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($category);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('postcat.flash.deleted'));

        return $this->redirectToRoute('admin_post_category_index');
    }

}