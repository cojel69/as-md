<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\ContactUs;
use AppBundle\Form\ContactUsType;

/**
 * Controller used to manage post's category in admin panel.
 *
 * @Route("admin/messages")
* @author Arnaud Anato <arnato1@gmail.com>
 */
class AdminContactController extends Controller
{
    /**
     * Lists all Contact entities.
     *
     * @Route("/non-lus", name="admin_contact_index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $contact = new ContactUs();
        $contacts = $this->getDoctrine()->getRepository(ContactUs::class)->findByState($contact::UNSEEN);

        return $this->render('admin/contact/contact_list.html.twig',
                            [
                                'contacts' => $contacts,
                                "unsee"=> true
                            ]);
    }

     /**
     * Lists all Contact entities.
     *
     * @Route("/lus", name="admin_contact_see")
     */
    public function seecontactAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $contact = new ContactUs();
        $contacts = $this->getDoctrine()->getRepository(ContactUs::class)->findByState($contact::SEEN);

        return $this->render('admin/contact/contact_list.html.twig',
                            [
                                'contacts' => $contacts,
                                "see"=> true
                        ]);
    }

    public function contact_notificationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $contact = new ContactUs();
        $contacts = $this->getDoctrine()->getRepository(ContactUs::class)->findByState($contact::UNSEEN);

        return $this->render('admin/contact/contact_notification.html.twig',[
            'contacts' => $contacts
        ]);
    }



    /**
     * Show Contact entities.
     *
     * @Route("/{id}/lire", name="admin_contact_show")
     */
    public function showAction(Request $request,ContactUs $contact)
    {   
        $em = $this->getDoctrine()->getManager();

        $contact->setAsSeen();

        $em->flush();

        return $this->render('admin/contact/contact_show.html.twig', [
            'contact' => $contact,
         ]);
    }


    /**
     * Delete a category entity.
     *
     * @Route("/{id}/supprimer", name="admin_contact_delete")
     */
    public function deleteAction(ContactUs $contact)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($contact);
        $em->flush();

        $this->addFlash('success', $this->get('translator')->trans('adress.flash.deleted'));

        return $this->redirectToRoute('admin_contact_index');
    }
}