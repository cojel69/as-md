<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DossierImage
 *
 * @ORM\Table(name="asso_dossier_image")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DossierImageRepository")
 */
class DossierImage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, unique=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="dossierImages")
     */
    private $posts;
    /**
     * Constructor
     */
    public function __construct()
    {
        //$this->post = new \Doctrine\Common\Collections\ArrayCollection();
    }

   

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return DossierImage
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Add post
     *
     * @param \AppBundle\Entity\Post $post
     *
     * @return DossierImage
     
    public function addPost(\AppBundle\Entity\Post $post)
    {
        
        $this->posts[] = $post;

        return $this;
    }*/

    /**
     * Remove post
     *
     * @param \AppBundle\Entity\Post $post
     
    public function removePost(\AppBundle\Entity\Post $post)
    {
        $this->posts->removeElement($post);
    }*/

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * Set posts
     *
     * @param \AppBundle\Entity\Post $posts
     *
     * @return DossierImage
     */
    public function setPosts(\AppBundle\Entity\Post $posts = null)
    {
        $this->posts = $posts;

        return $this;
    }
}
