<?php
 
namespace AppBundle\Entity;
 
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
 
/**
 * Image
 *
 * @ORM\Table(name="asso_image")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ImageRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Image
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
     
    /**
     * @var string $extension
     *
     * @ORM\Column(name="extension", type="string", length=255)
     */
    private $extension;
 
    /**
     * @var string $alt
     *
     * @ORM\Column(name="alt", type="string", length=255)
     */
    private $alt;
     
    private $file;
     
    private $tempFilename;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set extension
     *
     * @param string $extension
     *
     * @return Image
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set alt
     *
     * @param string $alt
     *
     * @return Image
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt
     *
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }


    public function setFile(File $file)
    {
        $this->file = $file;
        if (null !== $this->extension) {
            $this->tempFilename = $this->extension;
 
            $this->extension = null;
            $this->alt = null;
        }
    }
 
    public function getFile()
    {
        return $this->file;
    }
     
     
    /**
    * @ORM\PrePersist()
    * @ORM\PreUpdate()
    */
    public function preUpload()
    {
        //dump($this->portfolio->getId()); die('prepersist image');
        if (null === $this->file) {
            return;
        }
        $this->extension = uniqid().'.'.$this->file->guessExtension();
 
        $this->alt = $this->file->getClientOriginalName();
     
    }
 
    /**
    * @ORM\PostPersist()
    * @ORM\PostUpdate()
    */
    public function upload()
    { 
        if (null === $this->file) {
            return;
        }

        if (null !== $this->tempFilename) {
            $oldFile = $this->getUploadRootDir().'/'.$this->id.'.'.$this->tempFilename;
            if (file_exists($oldFile)) {
                unlink($oldFile);
            }
        }
        //dump($this->getUploadRootDir().'.'.$this->id.'.'.$this->extension); die();
        $this->file->move($this->getUploadRootDir(),$this->getExtension());
        $this->file = null;
    }
 
    /**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload()
    {
        $this->tempFilename = $this->getUploadRootDir().'/'.$this->id.'.'.$this->extension;
    }
 
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if (file_exists($this->tempFilename)) {
            unlink($this->tempFilename);
        }
    }
 
    public function getUploadDir()
    {
        return 'upload/img';
    }
 
    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }
     
    public function getWebPath()
    {
        return $this->getUploadDir().'/'.$this->getExtension();
    }

}
