<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\ImageType;
/*use Symfony\Component\Form\Extension\Core\Type\SubmitType;
*/use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use FM\ElfinderBundle\Form\Type\ElFinderType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Intl;

class PostType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('title', TextType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'label.title',
                ])
           /* ->add('locale', ChoiceType::class, [
                         'choices' => array(
                'English' => 'en',
                'frensh' => 'fr',
            ),
            "mapped"=>false,
            ])*/
             ->add('category', EntityType::class,[
                'class' => 'AppBundle\Entity\Category',
                'choice_label' => 'name',
                'label' => 'label.category',
                'required' => false,
                'placeholder' => 'category.placeholder',
                'empty_data'  => null,
                ])
              /*->add('tags', EntityType::class,[
                'class' => 'AppBundle\Entity\Tag',
                'choice_label' => 'name',
                'label' => 'label.tag',
                'required' => false,
                'multiple'=> true,
                'placeholder' => 'tag.placeholder',
                'empty_data'  => null,
                ])*/
            ->add('content', CKEditorType::class, [
                'label' => 'label.content',
                'config_name' => 'default',
                ])
            ->add('dossierImages', CollectionType::class, [
                    'entry_type' => DossierImageType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => false,
                    'constraints' => new Valid(),
                    'empty_data'  => null
                   ])
            ->add('state', ChoiceType::class, array(
                'label'=>'label.state',
            'choices' => array(
            'post.state.bubble' => "bubble",
            'post.state.production' => "production",
            ),
            ))
           ->add('publishedAt', DateTimeType::class,[
                'label' => 'label.publishedAt',
               'widget' => 'single_text',
               'attr' => ['class' => 'datepicker'],
               'auto_initialize'=> "now"
                ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
        ->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Post',

        ))
        ;
    }
}
