<?php
/*
 *
 * (c) DevflamTech <http://devflamtech.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace AppBundle\Twig\Extension;

/**
 * Executes the die() function in twig template.
 *
 * @author Prudence asssogba <prudence@devflamtech.com>
 */

class EncodageExtension extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('encoder', array($this,'encodeRender')),
        );
    }

    /**
     * @param string|empty $message, string $type
     */
    public function encodeRender($message='',$type = 'sha1')
    {
        if ($type=="sha1") {
           return sha1($message);
        }
        if ($type=="md5") {
           return md5($message);
        }

        if ($type=="hash") {
           return hash($message);
        }

        return $message;
    }

     public function getName()
    {
        return 'app_twig_encodage_extension';
    }

}