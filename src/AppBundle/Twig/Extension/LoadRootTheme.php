<?php
namespace AppBundle\Twig\Extension;
use Doctrine\Common\Persistence\ObjectManager;

 /**
 *
 * @author Prudence Assogba <jprud67@gmail.com>
 */
class LoadRootTheme extends \Twig_Extension
{
    /**
    *@var ObjectManager
    */
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    public function getFilters()
    {
        return array(
            'LoadRootTheme' => new \Twig_SimpleFilter('LoadRootTheme', array($this, 'lrt'))
        );
    }
 
    public function lrt($string)
    {
        $theme_curent=$this->manager->getRepository('AppBundle:ActiveTemplate')->find(1);
        $folder=$theme_curent->getTemplate();
        return 'template/'.$folder.'/'.$string;
    }
 
    public function getName()
    {
        return 'load_root_theme_extension';
    }
 
}