<?php
/*
 *
 * (c) DevflamTech <http://devflamtech.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
namespace AppBundle\Twig\Extension;

/**
 * Executes the die() function in twig template.
 *
 * @author Prudence asssogba <prudence@devflamtech.com>
 */

class DieExtension extends \Twig_Extension
{
    /**
     * @string
     */
    private $environment;

    /**
     * @param string $environment
     */
    public function __construct($environment)
    {
        $this->environment = $environment;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('die', array($this,'killRender')),
        );
    }

    /**
     * @param string|null $message
     */
    public function killRender($message = null)
    {
        if ('dev' === $this->environment) {
            die($message);
        }

        return '';
    }

     public function getName()
    {
        return 'app_twig_dev_extension';
    }

}