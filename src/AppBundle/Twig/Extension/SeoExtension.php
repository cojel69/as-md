<?php
/*
 *
 * (c) DevflamTech <http://devflamtech.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace AppBundle\Twig\Extension;

use Doctrine\Common\Persistence\ObjectManager;

/**
 * Executes the seo() function in twig template.
 *
 * @author Prudence asssogba <prudence@devflamtech.com>
 */

class SeoExtension extends \Twig_Extension
{
    /**
     * @string
     */
    private $environment;

     /**
    *@var ObjectManager
    */
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('seo', array($this,'seo_load')),
        );
    }

    /**
     * @param string|null $message
     */
    public function seo_load($attach)
    {
        $attach=sha1($attach);
        if (!empty($attach)) {
            $datas=$this->manager->getRepository('AppBundle:Seo')->findOneBy(["attach"=>$attach]);
            if ($datas) {
               
                echo  '<meta name="keywords" content="'.$datas->getKeywords().'" /><meta name="description" content="'.$datas->getDescription().'" /><meta name="robots" content="'.$datas->getRobots().'" /><meta name="author" content="'.$datas->getAuthor().'">';
            }
            else
            {
                 $datas=$this->manager->getRepository('AppBundle:Seo')->find(1);

                 echo  '<meta name="keywords" content="'.$datas->getKeywords().'" /><meta name="description" content="'.$datas->getDescription().'" /><meta name="robots" content="'.$datas->getRobots().'" /><meta name="author" content="'.$datas->getAuthor().'">';
            }
        }
        else
        {
            return ' ';
        }
    }

     public function getName()
    {
        return 'app_twig_seo_extension';
    }

}