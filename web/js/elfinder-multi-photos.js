( function(){ 

	var index = $("#dsi-content div.col-md-3").length;
	var dossierId = "post_dossierImages_0_url";
	photoCount();


	function add() {
	    $("#dsi-content").append($('#nl').html().replace(/\_\_name\_\_/g, index));
	    dossierId = "post_dossierImages_" + index + "_url";
	    var childWin = window.open("/assymf/web/app_dev.php/elfinder/form?id=" + dossierId + "", "popupWindow", "height=450, width=900");


	}

	$(document).on("change", 'input[data-type="elfinder-input-field"]', function(e) {
		
	    var img = $('<div><a class="btn btn-default btn-xs delete-row"><i class="fa fa-trash"></i></a></div><img src="' + e.target.value + '" alt="" height=120 class="img-responsive" />');
		
	    $(this).closest('div.col-md-3').append(img);
	    photoCount();
	});

	$("#addDossierImage").click(function() {
	    var counter = $("#dsi-content div.col-md-3").length;
	    // limit uploads
	    if (counter < 20) {
	        add();
	        //elFinderCall();
	    }

	    index = index + 1;
	});

	$(document).on("click", "form a.delete-row", function() {
	    $(this).closest('div.col-md-3').fadeOut("slow").remove();
	});

	function photoCount() {
		var photoCounter = $("#dsi-content div.col-md-3").length;
		$('.photoCount').text(photoCounter);
	}


})();

function setValue(value, element_id) {
	$('[data-type="elfinder-input-field"]' + (element_id ? '[id="' + element_id + '"]' : '')).val(value).change();
}